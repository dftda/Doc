# 04/11/2020 : Réunion de projet n°2
**Sujet : Mise en place de benchmark sur les technologie + mise en place des premières phases**
Durée : 22h - 23h20
**Présent :**
    - Samuel (Chef de projet)
    - Jarod
    - Vincent
    - Morgan

### Documentation à faire pour la prochaine réunion : 
NestJS -> Jarod
MongoDB -> Sam
Angular -> Morgan, Jarod, Vincent, Sam
NativeScript -> Morgan, Jarod, Vincent, Sam
Test -> Sam
NodeJS -> Vincent, Sam

Phase 1,2,3,4 : Sam,Morgan,Vincent

Doc type :
- Présentation
- Installation
- Mise en marche (Faire un hello world)
- Utilisation
- Sitographoie

**Plannification Deuxième réunion le 14-15-16 Novembre**
     - Style matérial pour l'app
    - Commencer un wireframming
    - Fixer des objectifs de version
    
# 03/11/2020 : Réunion de projet n°1
**Sujet : Mise au point sur le logo**
Durée : 1h
**Présent** :
     - Lyliah (Designer & Developpeur)
     - Samuel  (Chef de projet)

# 30 / 10/2020 : Commité de projet n°1
#### Sujet : QQOQCCP + doc github + creation organisation sur github
Durée : 2h
**Présent :**
- Vincent (Developpeur)
- Morgan (Developpeur)
- Samuel  (Chef de projet)



